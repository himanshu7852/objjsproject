function assignDefaultValues(testObject, defaultObject)
{
    for(let index in testObject)
    {
        if(testObject[index]===undefined||testObject[index]===null)
        {
            testObject[index]=defaultObject[index];
        }
    }
    return testObject;
}

module.exports=assignDefaultValues;