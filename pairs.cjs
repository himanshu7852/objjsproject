function pairsOfObject(testObject)
{
    const pairs=[];
    for(let index in testObject)
    {
        let tempArr=[];
        tempArr.push(index);
        tempArr.push(testObject[index])
        pairs.push(tempArr);
    }
    return pairs;
}
module.exports=pairsOfObject;