function getValues(testObject)
{
    let values=[];
    for(let index in testObject)
    {
        if(typeof testObject[index]!=='function')
        {
            values.push(testObject[index]);
        }
    }
    return values;
}
module.exports=getValues;