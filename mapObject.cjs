let getCallBackFun=require('./test/testMapObject.cjs');

function mapObjFun(testObject, callBackFunction){
    for(let index in testObject){
        testObject[index] = callBackFunction(testObject[index]);
    }
    return testObject;
}
function callBackFunction(argument){
    return argument + 200;
}
module.exports=mapObjFun;
