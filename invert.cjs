
function getInvertedObject(testObject)
{
    let inverted={};
    for(let index in testObject)
    {
        inverted[testObject[index]]=index;
    }
    return inverted;
}
module.exports=getInvertedObject;